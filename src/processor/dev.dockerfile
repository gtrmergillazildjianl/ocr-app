FROM golang:1.15.6-alpine3.12


# In case you face TESSDATA_PREFIX error, you minght need to set env vars
# to specify the directory where "tessdata" is located.
ENV TESSDATA_PREFIX=/usr/share/tessdata

RUN apk update && \
    apk add git tesseract-ocr tesseract-ocr-dev leptonica leptonica-dev \
    tesseract-ocr-data-deu tesseract-ocr-data-jpn \
    gcc g++
# ref: https://github.com/otiai10/gosseract/blob/main/Dockerfile
#RUN apt-get update -qq && \
#    apt-get install -y -qq libtesseract-dev libleptonica-dev

# Load languages.
# These {lang}.traineddata would b located under ${TESSDATA_PREFIX}/tessdata.
#RUN apt-get install -y -qq \
#  tesseract-ocr-eng \
#  tesseract-ocr-deu \
#  tesseract-ocr-jpn

#RUN apt-get install git
# See https://github.com/tesseract-ocr/tessdata for the list of available languages.
# If you want to download these traineddata via `wget`, don't forget to locate
# downloaded traineddata under ${TESSDATA_PREFIX}/tessdata.

RUN go get github.com/canthefason/go-watcher && \
    go install github.com/canthefason/go-watcher/cmd/watcher

RUN go get -t github.com/otiai10/gosseract

ENTRYPOINT watcher