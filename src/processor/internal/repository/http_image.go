package repository

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"ocrapp/src/processor/internal/conf"
)

// HTTPImageRepository image downloader using http protocol
type HTTPImageRepository struct {
	client *http.Client
	host   string
}

// NewHTTPImageRepository creates new HTTPImageRepository
func NewHTTPImageRepository() ImageRepository {
	return &HTTPImageRepository{
		client: &http.Client{},
		host:   conf.GetConfig().ImageProvider.Host,
	}
}

// GetImage get image
func (repo *HTTPImageRepository) GetImage(imageID string) ([]byte, error) {
	resp, err := repo.client.Get(fmt.Sprintf("http://%s/image/%s", repo.host, imageID))
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
