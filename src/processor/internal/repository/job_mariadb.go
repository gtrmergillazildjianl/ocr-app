package repository

import (
	"database/sql"
	"log"
	"ocrapp/src/processor/internal/model"
	"time"

	// import for side effect
	_ "github.com/go-sql-driver/mysql"
)

// JobMariaDBRepository mariadb implementatio of job repository
type JobMariaDBRepository struct {
	dsn string
}

// NewJobMariaDBRepository creates new mariadb repository
func NewJobMariaDBRepository() JobRepository {
	return &JobMariaDBRepository{
		dsn: getMariaDBDSN(),
	}
}

// UpdateJob updates job
func (repo *JobMariaDBRepository) UpdateJob(job model.Job) (model.Job, error) {
	db := repo.openDatabase()
	defer db.Close()

	stmt, err := db.Prepare("UPDATE t_job SET started=?, status=?, completed=?, result=? WHERE id=?")
	if err != nil {
		return job, err
	}

	_, err = stmt.Exec(job.Started, job.Status, job.Completed, job.Result, job.ID)
	if err != nil {
		return job, err
	}

	return job, nil

}

func (repo *JobMariaDBRepository) openDatabase() *sql.DB {
	db, err := sql.Open(mariadbDriverName, repo.dsn)
	if err != nil {
		log.Fatalf("Failed to open database:\n%s", err)
	}
	db.SetConnMaxIdleTime(time.Minute * 3)
	return db
}
