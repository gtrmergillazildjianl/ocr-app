package repository

// ImageRepository image repository
type ImageRepository interface {
	GetImage(imageID string) ([]byte, error)
}
