package service

import (
	"encoding/json"
	"log"
	"ocrapp/src/processor/internal/model"
	"ocrapp/src/processor/internal/publisher"
	"ocrapp/src/processor/internal/repository"
	"time"

	"github.com/otiai10/gosseract/v2"
)

// OCRService ocr service
type OCRService struct {
	jobRepo   repository.JobRepository
	eventPub  publisher.EventPublisher
	imageRepo repository.ImageRepository
}

// NewOCRService creates new ocr service
func NewOCRService() *OCRService {
	return &OCRService{
		eventPub: nil,
		jobRepo:  nil,
	}
}

// WithJobRepository sets job repository
func (service *OCRService) WithJobRepository(repo repository.JobRepository) *OCRService {
	service.jobRepo = repo
	return service
}

// WithImageRepository sets event publisher
func (service *OCRService) WithImageRepository(repo repository.ImageRepository) *OCRService {
	service.imageRepo = repo
	return service
}

// WithEventPublisher sets event publisher
func (service *OCRService) WithEventPublisher(pub publisher.EventPublisher) *OCRService {
	service.eventPub = pub
	return service
}

// ProcessJob extract text from job, then update database and client
func (service *OCRService) ProcessJob(job model.Job) (model.Job, error) {
	job.Started = time.Now().Format("2006-01-02 15:04:05")
	job.Status = model.JobEventTypeInProgress
	service.eventPub.PublishJobEvent(job.UserID, model.JobEvent{
		Type: model.JobEventTypeInProgress,
		Job:  job,
	})
	_, err := service.jobRepo.UpdateJob(job)
	if err != nil {
		log.Fatal(err)
	}

	imageBytes, err := service.imageRepo.GetImage(job.ImageID)
	if err != nil {
		service.updateJobFailed(job)
		return job, err
	}

	ocrClient := gosseract.NewClient()
	err = ocrClient.SetImageFromBytes(imageBytes)
	if err != nil {
		service.updateJobFailed(job)
		return job, err
	}

	result, err := ocrClient.Text()
	ocrClient.Close()
	if err != nil {
		service.updateJobFailed(job)
		return job, err
	}

	job.Result = result
	job.Status = model.JobEventTypeSuccess
	job.Completed = time.Now().Format("2006-01-02 15:04:05")
	service.eventPub.PublishJobEvent(job.UserID, model.JobEvent{
		Type: model.JobEventTypeSuccess,
		Job:  job,
	})
	service.jobRepo.UpdateJob(job)
	debugOut, _ := json.Marshal(job)
	log.Println(string(debugOut))
	return job, nil
}

func (service *OCRService) updateJobFailed(job model.Job) {
	job.Status = model.JobStatusFailed
	job.Completed = time.Now().Format("2006-01-02 15:04:05")
	service.eventPub.PublishJobEvent(job.UserID, model.JobEvent{
		Type: model.JobEventTypeFailed,
		Job:  job,
	})
	service.jobRepo.UpdateJob(job)
}
