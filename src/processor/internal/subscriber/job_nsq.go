package subscriber

import (
	"ocrapp/src/processor/internal/conf"
	"ocrapp/src/processor/internal/subscriber/handler"

	"github.com/nsqio/go-nsq"
)

// JobNSQConsumer nsq implementation of job consumer
type JobNSQConsumer struct {
	consumer *nsq.Consumer
}

const (
	jobTopic   = "ocrapp_job"
	jobChannel = "process"
)

// NewJobNSQConsumer create new JobNSQConsumer
func NewJobNSQConsumer() *JobNSQConsumer {
	jobConsumer := &JobNSQConsumer{
		consumer: newNSQConsumer(jobTopic, jobChannel),
	}
	jobConsumer.bindHandlers()
	return jobConsumer
}

// Start start processing jobs
func (cons *JobNSQConsumer) Start() error {

	return cons.consumer.ConnectToNSQLookupds(conf.GetConfig().NSQ.NSQLookupDHosts)
}
func (cons *JobNSQConsumer) bindHandlers() {
	//cons.consumer.AddHandler(nsq.HandlerFunc(handler.ProcessJob))
	cons.consumer.AddConcurrentHandlers(nsq.HandlerFunc(handler.ProcessJob), 1)
}
