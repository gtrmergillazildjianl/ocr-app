package handler

import (
	"encoding/json"
	"log"
	"ocrapp/src/processor/internal/model"
	"ocrapp/src/processor/internal/publisher"
	"ocrapp/src/processor/internal/repository"
	"ocrapp/src/processor/internal/service"

	"github.com/nsqio/go-nsq"
)

// ProcessJob process job
func ProcessJob(message *nsq.Message) error {

	job := model.Job{}
	err := json.Unmarshal(message.Body, &job)

	if err != nil {
		log.Printf("Invalid job data %s:\n%s\n", message.Body, err)
		return nil
	}
	log.Printf("Received Message: %s\n", message.Body)

	result, err := service.NewOCRService().
		WithJobRepository(repository.NewJobMariaDBRepository()).
		WithEventPublisher(publisher.NewEventNSQPublisher()).
		WithImageRepository(repository.NewHTTPImageRepository()).
		ProcessJob(job)
	if err != nil {
		log.Printf("Failed to extract texts from job: %s\n%s\n", message.Body, err)
		return err
	}
	resultData, _ := json.Marshal(result)
	log.Println(string(resultData))
	return nil
}
