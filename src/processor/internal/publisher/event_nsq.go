package publisher

import (
	"encoding/json"
	"ocrapp/src/processor/internal/model"
	"strconv"
	"time"
)

// EventNSQPublisher event publisher nsq implementation
type EventNSQPublisher struct {
	topicSuffix string
}

const (
	topicSuffix = "_ocrapp_event#ephemeral"
)

// NewEventNSQPublisher returns new event nsq publisher
func NewEventNSQPublisher() *EventNSQPublisher {
	return &EventNSQPublisher{
		topicSuffix: topicSuffix,
	}
}

// PublishJobEvent publishes job event
func (publisher *EventNSQPublisher) PublishJobEvent(userID string, event model.JobEvent) error {
	producer := newNSQProducer()
	defer producer.Stop()

	event.TimeStamp = strconv.FormatInt(time.Now().UnixNano(), 10)
	payload, err := json.Marshal(event)
	if err != nil {
		return err
	}

	return producer.Publish(userID+publisher.topicSuffix, payload)
}
