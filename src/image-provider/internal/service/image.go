package service

import (
	"bytes"
	"image"
	"io/ioutil"
	"math"
	"strconv"
	"strings"

	"errors"
	// import for sideffect
	_ "image/jpeg"
	"image/png"
	"log"
	img_proc "ocrapp/src/image-provider/pkg/image"
)

// ImageProcessorService processor
type ImageProcessorService struct {
	ops []string
}

// NewImageProcessorService creates new image processor
func NewImageProcessorService(ops ...string) *ImageProcessorService {
	return &ImageProcessorService{
		ops: ops,
	}
}

// ProcessImage process image
func (processor *ImageProcessorService) ProcessImage(raw []byte) ([]byte, error) {

	img, _, err := image.Decode(bytes.NewReader(raw))
	if err != nil {
		return nil, err
	}

	img, err = processor.doOperations(img)
	if err != nil {
		return nil, err
	}

	newBuffer := bytes.NewBuffer([]byte{})
	png.Encode(newBuffer, img)

	return ioutil.ReadAll(newBuffer)
}

func (processor *ImageProcessorService) doOperations(raw image.Image) (image.Image, error) {
	img := raw
	var err error
	for _, op := range processor.ops {
		img, err = processor.doOperation(img, op)
		if err != nil {
			return nil, err
		}
	}
	return img, nil
}

func (processor *ImageProcessorService) doOperation(raw image.Image, op string) (image.Image, error) {
	opSplitted := strings.Split(op, "_")

	switch opSplitted[0] {
	case "rs":
		return processor.doResizeOperation(raw, opSplitted[1])
	case "gs":
		return processor.doGrayScaleOperation(raw)
	case "bb":
		return processor.doBoxBlurOperation(raw)
	case "gb":
		return processor.doGaussianBlurOperation(raw, opSplitted[1:]...)
	case "b":
		return processor.doBinarize(raw, opSplitted[1])
	case "ed":
		return processor.doEdgeDetect(raw)
	default:
		return nil, errors.New("Unknown operation " + op)
	}
}

// DoOperation does the operation
func (processor *ImageProcessorService) DoOperation(data []byte, op string) ([]byte, error) {
	raw, _, err := image.Decode(bytes.NewReader(data))
	var out image.Image
	if err != nil {
		return nil, err
	}

	opSplitted := strings.Split(op, "_")
	switch opSplitted[0] {
	case "rs":
		out, err = processor.doResizeOperation(raw, opSplitted[1])
	case "gs":
		out, err = processor.doGrayScaleOperation(raw)
	case "bb":
		out, err = processor.doBoxBlurOperation(raw)
	case "gb":
		out, err = processor.doGaussianBlurOperation(raw, opSplitted[1:]...)
	case "b":
		out, err = processor.doBinarize(raw, opSplitted[1])
	case "ed":
		out, err = processor.doEdgeDetect(raw)
	default:
		return nil, errors.New("Unknown operation " + op)
	}

	if err != nil {
		return nil, err
	}

	newBuffer := bytes.NewBuffer([]byte{})
	png.Encode(newBuffer, out)
	return ioutil.ReadAll(newBuffer)
}

// param
// 		333w - resize width to 333pixels  maintaining aspect ratio
// 		333h - resize height to 333pixels maintaining aspect ratio
func (processor *ImageProcessorService) doResizeOperation(img image.Image, param string) (image.Image, error) {

	var size int64
	sizeString := strings.FieldsFunc(param, func(c rune) bool {
		return string(c) == "w" || string(c) == "h"
	})[0]

	size, err := strconv.ParseInt(sizeString, 10, 64)
	if err != nil {
		return img, err
	}

	size = int64(math.Min(float64(size), 2000))

	origWidth, origHeight := float64(img.Bounds().Dx()), float64(img.Bounds().Dy())
	ratio := origWidth / origHeight

	if strings.HasSuffix(param, "h") {
		return img_proc.Resize(img, int(float64(size)*ratio), int(size)), nil
	}
	return img_proc.Resize(img, int(size), int(float64(size)/ratio)), nil
}

func (processor *ImageProcessorService) doGrayScaleOperation(img image.Image) (image.Image, error) {
	log.Println("doing grayscale")
	return img_proc.GrayScale(img), nil
}

func (processor *ImageProcessorService) doBoxBlurOperation(img image.Image) (image.Image, error) {
	log.Println("doing box blur")
	return img_proc.BoxBlur(img, 10), nil
}

// params
// 	gb_10_3x
//  gb_10_5x
func (processor *ImageProcessorService) doGaussianBlurOperation(img image.Image, params ...string) (image.Image, error) {
	log.Println("doing gaussian blur")
	iteration := 5
	dim := img_proc.Gaussian3x3

	for _, value := range params {
		if strings.HasSuffix(value, "x") {
			newDim, err := strconv.ParseUint(strings.Trim(value, "x"), 10, 64)
			if err != nil {
				return nil, err
			}
			if img_proc.GaussianDim(newDim) == img_proc.Gaussian5x5 {
				dim = img_proc.Gaussian5x5
			}
		} else {
			newIteration, err := strconv.ParseUint(value, 10, 64)
			if err != nil {
				return nil, err
			}
			iteration = int(math.Min(float64(newIteration), 20))
		}
	}

	return img_proc.GaussianBlur(img, iteration, dim), nil
}

// param
//   b_255
func (processor *ImageProcessorService) doBinarize(img image.Image, param string) (image.Image, error) {
	log.Println("Binarizing image")
	threshold, err := strconv.ParseUint(param, 10, 64)
	if err != nil {
		return nil, err
	}

	return img_proc.Binarize(img, uint8(math.Min(float64(threshold), float64(255)))), nil
}

func (processor *ImageProcessorService) doEdgeDetect(img image.Image) (image.Image, error) {
	return img_proc.EdgeDetect(img), nil
}
