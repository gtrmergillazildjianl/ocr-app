package repository

// ImageRepository image repository
type ImageRepository interface {
	GetImage(path string) ([]byte, error)
}
