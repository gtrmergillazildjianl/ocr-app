package conf

import (
	"io/ioutil"
	"log"
	"sync"

	"gopkg.in/yaml.v2"
)

// AppConfig app configuration struct
type AppConfig struct {
	ListenAddress string `yaml:"listenAddress"`
	CacheBytes    int64  `yaml:"cacheBytes"`
	Pool          struct {
		Self  string   `yaml:"self"`
		Peers []string `yaml:"peers"`
	} `yaml:"pool"`
	Minio struct {
		Host            string `yaml:"host"`
		AccessID        string `yaml:"accessID"`
		SecretAccessKey string `yaml:"secretAccessKey"`
		Bucket          string `yaml:"bucket"`
	} `yaml:"minio"`
}

var (
	config *AppConfig
	once   sync.Once
)

// LoadConfig loads configuration
func LoadConfig(path string) *AppConfig {
	once.Do(func() {
		data, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatalf("Failed to load configuration file(%s):\n%s\n", path, err)
		}
		err = yaml.Unmarshal(data, &config)

		if err != nil {
			log.Fatalf("Failed to parse configuration file(%s):\n%s\n%s\n", path, string(data), err)
		}
		log.Printf("Loaded Configuration:\n%s\n", string(data))
	})
	return config
}

// GetConfig returns configuration
func GetConfig() *AppConfig {
	if config == nil {
		log.Fatalf("Should call conf.LoadConfig() first before calling conf.GetConfig, please check your code")
	}
	return config
}
