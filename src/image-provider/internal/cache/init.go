package cache

import (
	"log"
	"ocrapp/src/image-provider/internal/conf"
	"os"
	"sync"

	"github.com/golang/groupcache"
)

var (
	initOnce sync.Once
	pool     *groupcache.HTTPPool
)

// InitGroupCache Groupcache init groupcache
func InitGroupCache() {
	initOnce.Do(func() {
		log.Println("init pool")
		config := conf.GetConfig()
		self, exists := os.LookupEnv("GC_SELF")

		if !exists {
			self = config.Pool.Self
		}

		pool = groupcache.NewHTTPPoolOpts(self, &groupcache.HTTPPoolOptions{})
		log.Println("self:" + self)
		pool.Set(config.Pool.Peers...)
		GetImageGroup()
	})
}

// GetPool returns pool
func GetPool() *groupcache.HTTPPool {
	if pool == nil {
		log.Fatal("pool is nil")
	}
	return pool
}
