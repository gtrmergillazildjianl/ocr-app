package handler

import (
	"context"
	"log"
	"net/http"

	"ocrapp/src/image-provider/internal/cache"

	"github.com/golang/groupcache"
	"github.com/gorilla/mux"
)

// GetCachedImage get cached image
func GetCachedImage(res http.ResponseWriter, req *http.Request) {
	imageID := mux.Vars(req)["imageID"]
	group := cache.GetImageGroup()
	data := []byte{}

	err := group.Get(context.Background(), imageID,
		groupcache.AllocatingByteSliceSink(&data))

	log.Println(group.Stats.PeerErrors)
	log.Println(group.Stats.PeerLoads)

	if err != nil {
		res.WriteHeader(500)
		res.Write([]byte(err.Error()))
		return
	}
	res.Header().Set("Cache-Control", "max-age=3600")
	res.Write(data)

}
