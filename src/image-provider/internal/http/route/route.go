package route

import (
	"net/http"
	"ocrapp/src/image-provider/internal/cache"
	"ocrapp/src/image-provider/internal/http/handler"

	"github.com/gorilla/mux"
)

// GetRouter returns new handler
func GetRouter() http.Handler {
	r := mux.NewRouter()
	r.Handle("/_groupcache/image/{imageID}", cache.GetPool())
	r.HandleFunc("/image/{imageID}", handler.GetCachedImage)
	r.HandleFunc("/health", handler.HealthCheck)

	return handler.AddLogging(r)
}
