package image

import "image"

// GaussianDim gaussian blur dimension
type GaussianDim int

const (
	// Gaussian3x3 use 3x3 kernel
	Gaussian3x3 GaussianDim = 3
	// Gaussian5x5 use 5x5 kernel
	Gaussian5x5 GaussianDim = 5
)

// BoxBlur apply box blur
func BoxBlur(img image.Image, iteration int) image.Image {

	kernel := [][]float64{
		{float64(1) / float64(9), float64(1) / float64(9), float64(1) / float64(9)},
		{float64(1) / float64(9), float64(1) / float64(9), float64(1) / float64(9)},
		{float64(1) / float64(9), float64(1) / float64(9), float64(1) / float64(9)},
	}

	blurred, _ := Convolve(img, kernel)
	for count := 0; count < iteration-1; count++ {
		blurred, _ = Convolve(blurred, kernel)
	}
	return blurred
}

// GaussianBlur apply gaussian blur
func GaussianBlur(img image.Image, iteration int, dim GaussianDim) image.Image {
	var kernel Kernel

	if dim == Gaussian5x5 {
		denom := float64(256)
		kernel = [][]float64{
			{float64(1) / denom, float64(4) / denom, float64(6) / denom, float64(4) / denom, float64(1) / denom},
			{float64(4) / denom, float64(16) / denom, float64(24) / denom, float64(16) / denom, float64(4) / denom},
			{float64(6) / denom, float64(24) / denom, float64(36) / denom, float64(24) / denom, float64(6) / denom},
			{float64(4) / denom, float64(16) / denom, float64(24) / denom, float64(16) / denom, float64(4) / denom},
			{float64(1) / denom, float64(4) / denom, float64(6) / denom, float64(4) / denom, float64(1) / denom},
		}
	} else {
		denom := float64(16)
		kernel = [][]float64{
			{float64(1) / denom, float64(2) / denom, float64(1) / denom},
			{float64(2) / denom, float64(4) / denom, float64(2) / denom},
			{float64(1) / denom, float64(2) / denom, float64(1) / denom},
		}
	}

	blurred, _ := Convolve(img, kernel)
	for count := 0; count < iteration-1; count++ {
		blurred, _ = Convolve(blurred, kernel)
	}

	return blurred

}
