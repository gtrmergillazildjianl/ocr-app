package image

import (
	"image"
)

// Resize returns resized image
func Resize(img image.Image, width int, height int) image.Image {
	resizedImg := image.NewRGBA(image.Rect(0, 0, width, height))
	a, b := float64(img.Bounds().Dx())/float64(width), float64(img.Bounds().Dy())/float64(height)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			resizedImg.Set(x, y, img.At(int(float64(x)*a), int(float64(y)*b)))
		}
	}

	return resizedImg
}
