package image

import (
	"errors"
	"image"
	"image/color"
	"math"
)

// Kernel kernel
type Kernel [][]float64

// Convolve apply convolution to image img using kernel k
func Convolve(img image.Image, k Kernel) (image.Image, error) {
	err := k.isValid()
	if err != nil {
		return nil, err
	}
	width, height := img.Bounds().Dx(), img.Bounds().Dy()
	convImage := image.NewRGBA64(image.Rect(0, 0, width, height))
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			convImage.Set(x, y, k.applyTo(&img, &x, &y))
		}
	}
	return convImage, nil
}

func (kernel *Kernel) isValid() error {
	if len(*kernel) != len((*kernel)[0]) {
		return errors.New("Invalid kernel: should be square")
	}

	if len(*kernel)%2 == 0 {
		return errors.New("Invalid Kernel: dimensions should be odd")
	}

	return nil
}

func (kernel Kernel) applyTo(img *image.Image, x *int, y *int) color.Color {

	var sumR, sumG, sumB float64
	kSize := len(kernel)
	kHalf := kSize / 2
	width, height := (*img).Bounds().Dx(), (*img).Bounds().Dy()
	for ky := 0; ky < kSize; ky++ {
		for kx := 0; kx < kSize; kx++ {
			var r, g, b uint32
			pixelX := ky - kHalf + *x
			pixelY := kx - kHalf + *y
			if pixelX < 0 || pixelX > width || pixelY < 0 || pixelY > height {
				r, g, b = 0, 0, 0
			} else {
				r, g, b, _ = (*img).At(pixelX, pixelY).RGBA()
			}
			sumR += (kernel[kSize-ky-1][kSize-kx-1] * float64(r))
			sumG += (kernel[kSize-ky-1][kSize-kx-1] * float64(g))
			sumB += (kernel[kSize-ky-1][kSize-kx-1] * float64(b))

		}
	}

	// if *x == width/2 && *y == height/2 {
	// 	log.Printf("sumR: %f", sumR)
	// 	log.Fatalf("r:%d ", 3)
	// }
	_, _, _, a := (*img).At(*x, *y).RGBA()
	return color.RGBA64{
		R: uint16(math.Max(sumR, 0)),
		G: uint16(math.Max(sumG, 0)),
		B: uint16(math.Max(sumB, 0)),
		A: uint16(a),
	}
}
