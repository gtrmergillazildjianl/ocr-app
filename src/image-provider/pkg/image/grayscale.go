package image

import (
	"image"
	"image/color"
)

// GrayScale apply grayscale to image
func GrayScale(img image.Image) image.Image {
	width, height := img.Bounds().Dx(), img.Bounds().Dy()
	grayScaleImg := image.NewRGBA64(image.Rect(0, 0, width, height))
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			r, g, b, a := img.At(x, y).RGBA()
			gray := uint16((float64(r) + float64(g) + float64(b)) / 3)
			grayScaleImg.Set(x, y, color.RGBA64{
				R: gray,
				G: gray,
				B: gray,
				A: uint16(a),
			})
		}
	}
	return grayScaleImg
}
