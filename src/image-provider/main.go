package main

import (
	"log"
	"net/http"
	"ocrapp/src/image-provider/internal/cache"
	"ocrapp/src/image-provider/internal/conf"
	"ocrapp/src/image-provider/internal/http/route"
	"os"
)

func main() {
	path := os.Args[1]
	config := conf.LoadConfig(path)
	cache.InitGroupCache()

	log.Fatal(http.ListenAndServe(config.ListenAddress, route.GetRouter()))
}
