package main

import (
	"log"
	"net/http"
	"os"

	"ocrapp/src/api/internal/http/route"

	"ocrapp/src/api/internal/conf"
)

func main() {
	config := conf.LoadConfig(os.Args[1])
	log.Fatal(http.ListenAndServe(config.ListenAddress, route.GetRouter()))
}
