FROM golang:1.15.6-alpine3.12

RUN apk update && \
    apk add git
RUN go get github.com/canthefason/go-watcher && \
    go install github.com/canthefason/go-watcher/cmd/watcher

ENTRYPOINT watcher