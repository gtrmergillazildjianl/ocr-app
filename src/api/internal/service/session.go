package service

import (
	"log"
	"ocrapp/src/api/internal/conf"
	"ocrapp/src/api/internal/errors"
	"ocrapp/src/api/internal/model"
	"ocrapp/src/api/internal/repository"
	"ocrapp/src/api/pkg/random"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// SessionService issue and expire token/session
type SessionService struct {
	sessionRepo repository.SessionRepository
}

// NewSessionService returns new instance of SessioService
func NewSessionService() SessionService {
	return SessionService{
		sessionRepo: nil,
	}
}

// WithSessionRepository sets the sessionRepository to use
func (service SessionService) WithSessionRepository(sessionRepo repository.SessionRepository) SessionService {
	service.sessionRepo = sessionRepo
	return service
}

// GetBearerToken returns bearer token
// expires sessionID stored then returns a new session id to be set on http cookie
func (service SessionService) GetBearerToken(sessionID string) (newSessionID string, bearer model.BearerToken, svcError errors.ServiceError) {

	sessionIDSlice := strings.Split(sessionID, ":")
	if len(sessionIDSlice) != 2 {
		svcError.Code = 403
		svcError.Message = "Invalid Session"
		return
	}

	userInfo, err := service.sessionRepo.GetSession(sessionIDSlice[0])

	if err != nil || userInfo.ID != sessionID {
		svcError.Code = 403
		svcError.Message = "Invalid Session"
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":  time.Now().Add(5 * time.Minute).Unix(),
		"iat":  time.Now().Unix(),
		"user": userInfo.User,
	})

	signed, err := token.SignedString([]byte(conf.GetConfig().HMACSecret))
	if err != nil {
		log.Println(err)
	}

	newSessionID = userInfo.User.GoogleID + ":" + random.GenerateBase64String(64)

	bearer = model.BearerToken{
		User:   userInfo.User,
		Bearer: signed,
	}

	service.sessionRepo.DestroySession(sessionIDSlice[0])
	service.sessionRepo.StoreSession(userInfo.User.GoogleID, model.Session{
		ID:    newSessionID,
		Token: userInfo.Token,
		User:  userInfo.User,
	})
	//	service.sessionRepo.StoreBearer(bearer.User.GoogleID, bearer)
	return
}

// Logout of the service
func (service SessionService) Logout(sessionID string) (svcError errors.ServiceError) {
	sessionIDSlice := strings.Split(sessionID, ":")
	if len(sessionIDSlice) != 2 {
		svcError.Code = 403
		svcError.Message = "Invalid Session"
		return
	}
	service.sessionRepo.DestroySession(sessionIDSlice[0])
	return
}
