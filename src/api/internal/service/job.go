package service

import (
	"fmt"
	"ocrapp/src/api/internal/errors"
	"ocrapp/src/api/internal/model"
	"ocrapp/src/api/internal/publisher"
	"ocrapp/src/api/internal/repository"
)

// JobService struct
type JobService struct {
	jobRepo  repository.JobRepository
	eventPub publisher.EventPublisher
}

// NewJobService returns a new JobService instance
func NewJobService() *JobService {
	return &JobService{
		jobRepo:  nil,
		eventPub: nil,
	}
}

// WithJobRepository sets the jobrepository
func (service *JobService) WithJobRepository(repo repository.JobRepository) *JobService {
	service.jobRepo = repo
	return service
}

// WithEventPublisher sets the event publisher
func (service *JobService) WithEventPublisher(pub publisher.EventPublisher) *JobService {
	service.eventPub = pub
	return service
}

// GetJob returns specific job for user
func (service *JobService) GetJob(userID string, jobID int64) (model.Job, errors.ServiceError) {
	job, err := service.jobRepo.GetJobByID(jobID)

	if err != nil {

		if err.Error() == "sql: no rows in result set" {
			return job, errors.ServiceError{
				Code:    404,
				Message: "Job does not exist",
			}
		}

		return job, errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}

	if job.UserID != userID {
		return job, errors.ServiceError{
			Code:    403,
			Message: "You do not own this job",
		}
	}

	return job, errors.ServiceError{}
}

// GetJobs returns
func (service *JobService) GetJobs(userID string, refJobID int64, n int64) ([]model.Job, errors.ServiceError) {
	jobs, err := service.jobRepo.GetJobsByUserID(userID, refJobID, n)
	if err != nil {
		return jobs, errors.ServiceError{
			Code:    500,
			Message: fmt.Sprintf("Failed JobService.(%s, %d, %d):\n%s\n", userID, refJobID, n, err),
		}
	}
	return jobs, errors.ServiceError{}
}

// DeleteJob deletes job
func (service *JobService) DeleteJob(userID string, jobID int64) (model.Job, errors.ServiceError) {
	job, svcErr := service.GetJob(userID, jobID)
	if svcErr.Code != 0 {
		return job, svcErr
	}
	_, err := service.jobRepo.DeleteJobByID(userID, jobID)
	if err != nil {
		return job, errors.ServiceError{
			Code:    500,
			Message: fmt.Sprintf("Server error due to JobService.DeleteJob(%s, %d):\n%s\n", userID, jobID, err.Error()),
		}
	}

	service.eventPub.PublishJobEvent(userID, model.JobEvent{
		Type: model.JobEventTypeDeleted,
		Job:  job,
	})
	return job, svcErr
}
