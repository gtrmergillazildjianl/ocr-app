package service

import (
	"crypto/sha512"
	"encoding/hex"
	"net/http"
	"ocrapp/src/api/internal/errors"
	"ocrapp/src/api/internal/model"
	"ocrapp/src/api/internal/publisher"
	"ocrapp/src/api/internal/repository"
	"time"
)

// ImageService image service struct
type ImageService struct {
	imageRepo      repository.ImageRepository
	jobRepo        repository.JobRepository
	jobPublisher   publisher.JobPublisher
	eventPublisher publisher.EventPublisher
}

// NewImageService returns new instance of ImageService
func NewImageService() ImageService {
	return ImageService{
		imageRepo: nil,
	}
}

// WithImageRepository sets the imageRepository to be used by the service
func (service ImageService) WithImageRepository(repo repository.ImageRepository) ImageService {
	service.imageRepo = repo
	return service
}

// WithJobRepository sets the jobRepository
func (service ImageService) WithJobRepository(repo repository.JobRepository) ImageService {
	service.jobRepo = repo
	return service
}

// WithJobPublisher sets the jobPublisher
func (service ImageService) WithJobPublisher(publisher publisher.JobPublisher) ImageService {
	service.jobPublisher = publisher
	return service
}

// WithEventPublisher sets the eventPublisher
func (service ImageService) WithEventPublisher(publisher publisher.EventPublisher) ImageService {
	service.eventPublisher = publisher
	return service
}

// SaveImage stores the image given by the user
// Validate if the image is valid
// return job information if the image is valid
// dispatch a job in nsq
// dispatch an event in nsq to notify event hub
func (service ImageService) SaveImage(userID string, name string, imageData []byte) (model.Job, errors.ServiceError) {
	job := model.Job{}
	err := errors.ServiceError{}

	imageID, err := service.saveImageToObjectStorage(userID, imageData)
	if err.Code != 0 {
		return job, err
	}

	job = model.Job{
		UserID:     userID,
		Name:       name,
		ImageID:    imageID,
		Status:     model.JobStatusPending,
		Dispatched: "0000-00-00 00:00:00",
		Started:    "0000-00-00 00:00:00",
		Completed:  "0000-00-00 00:00:00",
	}

	err = service.saveJobToDatabase(&job)
	if err.Code != 0 {
		return job, err
	}

	err = service.publishJob(&job)
	if err.Code != 0 {
		return job, err
	}

	service.jobRepo.UpdateJobByID(job.UserID, job.ID, job)

	return job, err
}

// save to object storage
func (service ImageService) saveImageToObjectStorage(userID string, imageData []byte) (string, errors.ServiceError) {
	svcError := errors.ServiceError{}

	contentType := http.DetectContentType(imageData)

	if contentType != "image/apng" && contentType != "image/jpeg" && contentType != "image/png" {
		return "", errors.ServiceError{
			Code:    400,
			Message: "Invalid Image: supported types(image/apng, image/jpeg, image/png)",
		}
	}

	hasher := sha512.New()
	hasher.Write(imageData)
	hash := hasher.Sum(nil)
	hashString := hex.EncodeToString(hash)
	err := service.imageRepo.StoreImage(hashString, imageData, contentType)
	if err != nil {
		return "", errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}
	return hashString, svcError
}

// save to database
func (service ImageService) saveJobToDatabase(job *model.Job) errors.ServiceError {
	svcErr := errors.ServiceError{}
	id, err := service.jobRepo.StoreJob(*job)

	if err != nil {
		service.imageRepo.DeleteImage(job.ImageID)

		service.eventPublisher.PublishJobEvent(job.UserID, model.JobEvent{
			Type: model.JobEventTypeFailed,
			Job:  *job,
		})

		return errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}
	job.ID = id
	return svcErr
}

// pubish to queue
func (service ImageService) publishJob(job *model.Job) errors.ServiceError {
	job.Dispatched = time.Now().Format("2006-01-02 15:04:05")
	err := service.jobPublisher.PublishJob(*job)

	if err != nil {
		job.Status = model.JobStatusFailed
		service.jobRepo.UpdateJobByID(job.UserID, job.ID, *job)
		service.eventPublisher.PublishJobEvent(job.UserID, model.JobEvent{
			Type: model.JobEventTypeFailed,
			Job:  *job,
		})
		return errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}

	service.eventPublisher.PublishJobEvent(job.UserID, model.JobEvent{
		Type: model.JobEventTypeDispatched,
		Job:  *job,
	})

	return errors.ServiceError{}
}
