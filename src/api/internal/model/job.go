package model

// Job struct
type Job struct {
	ID         int64  `json:"id"`
	UserID     string `json:"user_id"`
	Name       string `json:"name"`
	ImageID    string `json:"image_id"`
	Status     string `json:"status"`
	Dispatched string `json:"dispatched"`
	Started    string `json:"started"`
	Completed  string `json:"completed"`
	Result     string `json:"result"`
}

const (
	// JobStatusPending pending job
	JobStatusPending = "pending"
	// JobStatusInProgress job currently in progress
	JobStatusInProgress = "inprogress"
	// JobStatusDone job completed
	JobStatusDone = "done"
	// JobStatusFailed job failed
	JobStatusFailed = "failed"
)
