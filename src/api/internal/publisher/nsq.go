package publisher

import (
	"log"

	"ocrapp/src/api/internal/conf"

	"github.com/nsqio/go-nsq"
)

func newNSQProducer() *nsq.Producer {
	producer, err := nsq.NewProducer(conf.GetConfig().NSQ.Host, nsq.NewConfig())

	if err != nil {
		log.Fatalf("Failed to create nsq producer:\n%s\n", err)
	}
	return producer
}
