package publisher

import "ocrapp/src/api/internal/model"

// JobPublisher publishes job to message broker
type JobPublisher interface {
	PublishJob(job model.Job) error
}
