package errors

// ServiceError represents errors returned by services
type ServiceError struct {
	Code    int
	Message string
}

func (err ServiceError) Error() string {
	return err.Message
}
