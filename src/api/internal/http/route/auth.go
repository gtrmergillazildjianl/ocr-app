package route

import (
	"net/http"
	"ocrapp/src/api/internal/http/handler"

	"github.com/gorilla/mux"
)

func attachAuthRoutes(r *mux.Router) {
	r.HandleFunc("/google/web/redirect", handler.RedirectWebToGoogleAuth).Methods(http.MethodGet)
	r.HandleFunc("/google/web/token", handler.GetWebGoogleToken).Methods(http.MethodGet)
	r.HandleFunc("/web/bearer", handler.GetWebBearerToken).Methods(http.MethodGet)
	r.HandleFunc("/web/bearer", handler.Logout).Methods(http.MethodDelete)

}
