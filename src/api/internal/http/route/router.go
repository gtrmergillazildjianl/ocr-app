package route

import (
	"net/http"
	"ocrapp/src/api/internal/http/handler"

	"github.com/gorilla/mux"
)

// GetRouter returns api handler with configured routes
func GetRouter() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/health", handler.HealthCheck)
	attachJobRoutes(r.PathPrefix("/api/jobs").Subrouter())
	attachUploadRoutes(r.PathPrefix("/api/upload").Subrouter())
	attachAuthRoutes(r.PathPrefix("/api/auth").Subrouter())
	return r
}
