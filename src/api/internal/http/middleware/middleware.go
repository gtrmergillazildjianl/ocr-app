package middleware

import "net/http"

// Middleware type
type Middleware func(res http.ResponseWriter, req *http.Request, next http.HandlerFunc)

// WithMiddleware attaches middleware to route
func WithMiddleware(handler http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	if len(middlewares) == 0 {
		return handler
	}

	return buildCallStack(handler, middlewares...)
}

func buildCallStack(handler http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {

	current, middlewares := middlewares[0], middlewares[1:]

	if len(middlewares) == 0 {
		return func(res http.ResponseWriter, req *http.Request) {
			current(res, req, func(res http.ResponseWriter, req *http.Request) {
				handler(res, req)
			})
		}
	}

	return func(res http.ResponseWriter, req *http.Request) {
		current(res, req, func(res http.ResponseWriter, req *http.Request) {
			buildCallStack(handler, middlewares...)(res, req)
		})
	}

}

