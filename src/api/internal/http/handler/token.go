package handler

import (
	"net/http"
	"ocrapp/src/api/internal/repository"
	"ocrapp/src/api/internal/service"

	"ocrapp/src/api/internal/http/util"
)

// GetWebBearerToken returns bearer token to client and sets new sessionID
// @TODO
func GetWebBearerToken(res http.ResponseWriter, req *http.Request) {

	sessionIDCookie, cookieErr := req.Cookie("session_id")

	if cookieErr != nil {
		res.WriteHeader(403)
		return
	}

	newSessionID, bearer, err := service.NewSessionService().
		WithSessionRepository(repository.NewSessionRedisRepository()).
		GetBearerToken(sessionIDCookie.Value)

	if handleError(err, res, req) {
		return
	}

	setSessionIDCookie(newSessionID, res)
	util.JSONResponse(bearer, 200, res)

}

// Logout logout of the service
func Logout(w http.ResponseWriter, r *http.Request) {
	sessionIDCookie, cookieErr := r.Cookie("session_id")

	if cookieErr != nil {
		w.WriteHeader(403)
		return
	}

	err := service.NewSessionService().WithSessionRepository(repository.NewSessionRedisRepository()).Logout(sessionIDCookie.Value)
	if handleError(err, w, r) {
		return
	}
	w.WriteHeader(200)
}
