package handler

import "net/http"

// HealthCheck health check route
// return 200
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
}
