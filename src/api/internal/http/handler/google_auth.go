package handler

import (
	"net/http"

	"ocrapp/src/api/internal/conf"
	"ocrapp/src/api/internal/service"
	"ocrapp/src/api/pkg/random"

	"ocrapp/src/api/internal/repository"

	"github.com/google/uuid"
)

// RedirectWebToGoogleAuth redirects web client to google openid consent screen
func RedirectWebToGoogleAuth(res http.ResponseWriter, req *http.Request) {
	config := conf.GetConfig()
	state := random.GenerateBase64String(48)

	params := make(map[string]string)
	params["client_id"] = config.AuthConfig.Google.ClientID
	params["response_type"] = "code"
	params["redirect_uri"] = config.AuthConfig.Google.RedirectURI.Web.HTTP
	params["nonce"] = uuid.New().String()
	params["state"] = state
	//params["access_type"] = "offline"
	//params["prompt"] = "consent"
	for _, scope := range config.AuthConfig.Google.Scope {
		params["scope"] += scope + " "
	}

	redirectURI, err := service.
		NewGoogleOAuthService().
		WithStateRepository(repository.NewStateRedisRepositorysitory()).
		GetRedirectURI(params)

	if handleError(err, res, req) {
		return
	}

	res.Header().Add("Location", redirectURI)
	res.WriteHeader(302)

}

// GetWebGoogleToken returns sessionID to web client
func GetWebGoogleToken(res http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	config := conf.GetConfig()
	service := service.NewGoogleOAuthService().
		WithStateRepository(repository.
			NewStateRedisRepositorysitory()).
		WithTokenRepository(repository.
			NewGoogleTokenRepository()).
		WithSessionRepository(repository.NewSessionRedisRepository())
	sessionID, err := service.
		GetToken(
			query.Get("state"),
			query.Get("code"),
			config.AuthConfig.Google.ClientID,
			config.AuthConfig.Google.ClientSecret,
			config.AuthConfig.Google.RedirectURI.Web.HTTP,
		)

	if handleError(err, res, req) {
		return
	}

	setSessionIDCookie(sessionID, res)
	res.Header().Add("content-type", "text/html")
	script := "<script>window.onload = () => {window.opener.location.href='/'; window.close();}</script>"
	res.Write([]byte(script))

}
