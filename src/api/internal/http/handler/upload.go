package handler

import (
	"io/ioutil"
	"log"
	"net/http"
	"ocrapp/src/api/internal/http/middleware"
	"ocrapp/src/api/internal/publisher"
	"ocrapp/src/api/internal/repository"
	"ocrapp/src/api/internal/service"

	"ocrapp/src/api/internal/http/util"
)

// UploadFile handles file upload
// returns the job dispatched
func UploadFile(res http.ResponseWriter, req *http.Request) {
	userInfo := middleware.ResponseWriterWithVerifiedBearerOf(res).GetUserInfo()

	req.ParseMultipartForm(20000000)

	file, headers, err := req.FormFile("file")
	log.Println(headers.Filename)
	if err != nil {
		log.Println("Failed opening file")
		res.WriteHeader(500)
		return
	}

	defer file.Close()
	fileBytes, err := ioutil.ReadAll(file)

	if err != nil {
		log.Println("Failed reading file")
		res.WriteHeader(500)
		return
	}

	imageSvc := service.NewImageService().
		WithImageRepository(repository.NewImageMinioRepository()).
		WithJobRepository(repository.NewJobMariaDBRepository()).
		WithJobPublisher(publisher.NewJobNSQPublisher()).
		WithEventPublisher(publisher.NewEventNSQPublisher())

	job, svcErr := imageSvc.SaveImage(userInfo.GoogleID, headers.Filename, fileBytes)
	if handleError(svcErr, res, req) {
		return
	}
	util.JSONResponse(job, 201, res)
}
