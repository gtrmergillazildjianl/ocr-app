package repository

import (
	"encoding/json"
	"time"

	"ocrapp/src/api/internal/model"

	"github.com/go-redis/redis/v8"
)

var sessionBaseKey = "session"
var bearerBaseKey = "bearer"

// SessionRedisRepository session repository redis implementation
type SessionRedisRepository struct {
	redisClient *redis.Client
}

// NewSessionRedisRepository returns redis session repository
func NewSessionRedisRepository() *SessionRedisRepository {
	return &SessionRedisRepository{
		redisClient: getRedisClient(),
	}
}

// StoreSession in redis cache
func (repo *SessionRedisRepository) StoreSession(key string, sess model.Session) error {
	data, _ := json.Marshal(sess)

	status := repo.redisClient.Set(redisCtx, sessionBaseKey+":"+key, data, 24*time.Hour)
	if status.Err() != nil {
		return status.Err()
	}

	return nil
}

// GetSession from redis
func (repo *SessionRedisRepository) GetSession(key string) (model.Session, error) {
	sess := model.Session{}
	status := repo.redisClient.Get(redisCtx, sessionBaseKey+":"+key)

	if status.Err() != nil {
		return sess, status.Err()
	}

	bytes, err := status.Bytes()
	if err != nil {
		return sess, nil
	}

	json.Unmarshal(bytes, &sess)

	return sess, nil
}

// DestroySession deletes the session
func (repo *SessionRedisRepository) DestroySession(key string) error {
	result := repo.redisClient.Del(redisCtx, sessionBaseKey+":"+key)
	if result.Err() != nil {
		return result.Err()
	}
	return nil
}

// StoreBearer stores the bearer token to redis
// 5 minutes expiration
func (repo *SessionRedisRepository) StoreBearer(key string, bearer model.BearerToken) error {
	data, _ := json.Marshal(bearer)

	status := repo.redisClient.Set(redisCtx, bearerBaseKey+":"+key, data, 5*time.Minute)

	if status.Err() != nil {
		return status.Err()
	}
	return nil
}

// GetBearer returns the Bearer token
func (repo *SessionRedisRepository) GetBearer(key string) (model.BearerToken, error) {
	bearer := model.BearerToken{}
	data, err := repo.redisClient.Get(redisCtx, bearerBaseKey+":"+key).Bytes()
	if err != nil {
		return bearer, err
	}

	err = json.Unmarshal(data, &bearer)

	return bearer, err
}
