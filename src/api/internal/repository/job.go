package repository

import "ocrapp/src/api/internal/model"

// JobRepository job datasource
type JobRepository interface {
	StoreJob(job model.Job) (int64, error)
	GetJobByID(id int64) (model.Job, error)
	GetJobsByUserID(userID string, fromID int64, count int64) ([]model.Job, error)
	UpdateJobByID(userID string, id int64, job model.Job) (model.Job, error)
	DeleteJobByID(userID string, id int64) (int64, error)
	DeleteJobByImageID(imageID string) (int64, error)
}
