package repository

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

// GoogleTokenRepository token repository for google
type GoogleTokenRepository struct {
	baseEndpoint string
}

// NewGoogleTokenRepository returns tokenRepository for google
func NewGoogleTokenRepository() *GoogleTokenRepository {
	return &GoogleTokenRepository{
		baseEndpoint: "https://oauth2.googleapis.com/token",
	}
}

// GetToken requests google token
func (repo *GoogleTokenRepository) GetToken(params map[string]string) (string, error) {
	client := &http.Client{}

	payload, err := json.Marshal(params)

	if err != nil {
		log.Panicln(err)
	}

	request, err := http.NewRequest(http.MethodPost, repo.baseEndpoint, bytes.NewBuffer(payload))

	if err != nil {
		log.Panicln(err)
		return "", err
	}
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")

	response, err := client.Do(request)

	if err != nil {
		log.Panicln(err)
		return "", err
	}

	if response.StatusCode < 200 || response.StatusCode > 399 {
		body, _ := ioutil.ReadAll(response.Body)
		log.Println(string(body))
		return "", errors.New("Invalid response from auth server: " + strconv.Itoa(response.StatusCode))
	}

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Panicln(err)
		return "", err
	}

	return string(body), nil
}
