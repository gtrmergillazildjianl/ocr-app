package repository

import (
	"encoding/json"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

// StateRedisRepository Redis
type StateRedisRepository struct {
	client *redis.Client
}

const stateBaseKey = "googleauth"

// NewStateRedisRepositorysitory returns redis implementation of StateRepository
func NewStateRedisRepositorysitory() *StateRedisRepository {
	return &StateRedisRepository{
		client: getRedisClient(),
	}
}

// StoreState stores state
func (repo *StateRedisRepository) StoreState(key string, state map[string]string, expiration time.Duration) error {
	data, err := json.Marshal(state)
	if err != nil {
		return err
	}
	status := repo.client.Set(redisCtx, stateBaseKey+":"+key, data, expiration)
	err = status.Err()
	if err != nil {
		log.Panicln(err)
		return err
	}
	return nil
}

// GetState returns state
func (repo StateRedisRepository) GetState(key string) (map[string]string, error) {
	var state = make(map[string]string)
	result := repo.client.Get(redisCtx, stateBaseKey+":"+key)
	err := result.Err()
	if err != nil {
		return state, err
	}

	data, err := result.Bytes()
	if err != nil {
		return state, err
	}

	err = json.Unmarshal(data, &state)
	if err != nil {
		log.Panicln(err)
	}
	return state, nil
}

// DeleteState deletes state
func (repo StateRedisRepository) DeleteState(key string) error {
	result := repo.client.Del(redisCtx, stateBaseKey+":"+key)
	err := result.Err()
	if err != nil {
		return err
	}
	return nil
}
