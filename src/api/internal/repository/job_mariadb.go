package repository

import (
	"database/sql"
	"log"
	"ocrapp/src/api/internal/model"
	"time"

	// import for side effect
	_ "github.com/go-sql-driver/mysql"
)

// JobMariaDBRepository mariadb implementation of JobRepository
type JobMariaDBRepository struct {
	dsn string
}

// NewJobMariaDBRepository returns new mariadb job repository
func NewJobMariaDBRepository() *JobMariaDBRepository {
	return &JobMariaDBRepository{
		dsn: getMariaDBDSN(),
	}
}

// StoreJob store job
func (repo *JobMariaDBRepository) StoreJob(job model.Job) (int64, error) {
	db := repo.openDatabase()
	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO t_job(userID, name, imageID, status, dispatched, started, completed) VALUES(?, ?, ?, ?, ?, ?, ?)")
	//defer stmt.Close()
	if err != nil {
		return 0, err
	}
	log.Println(job.UserID)

	result, err := stmt.Exec(job.UserID, job.Name, job.ImageID, job.Status, job.Dispatched, job.Started, job.Completed)
	if err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

// GetJobByID get job by jobid
func (repo *JobMariaDBRepository) GetJobByID(id int64) (model.Job, error) {
	db := repo.openDatabase()
	defer db.Close()

	row := db.QueryRow("SELECT * FROM t_job WHERE id=?", id)

	if row.Err() != nil {
		return model.Job{}, row.Err()
	}

	var (
		userID     string
		imageID    string
		name       string
		status     string
		dispatched string
		started    string
		completed  string
		result     string
	)
	err := row.Scan(&id, &userID, &name, &imageID, &status, &dispatched, &started, &completed, &result)
	if err != nil {
		return model.Job{}, err
	}
	return model.Job{
		ID:         id,
		UserID:     userID,
		Name:       name,
		ImageID:    imageID,
		Status:     status,
		Dispatched: dispatched,
		Started:    started,
		Completed:  completed,
		Result:     result,
	}, nil
}

// GetJobsByUserID returns job owned by user
func (repo *JobMariaDBRepository) GetJobsByUserID(userID string, fromID int64, count int64) ([]model.Job, error) {
	db := repo.openDatabase()
	defer db.Close()

	jobs := []model.Job{}
	var (
		rows *sql.Rows
		err  error
	)
	if fromID == -1 {
		rows, err = db.Query("SELECT * FROM t_job WHERE userID=? ORDER BY id DESC LIMIT 0,? ", userID, count)
	} else {
		rows, err = db.Query("SELECT * FROM t_job WHERE id<=? AND userID=? ORDER BY id DESC LIMIT 0,?", fromID, userID, count)
	}
	if err != nil {
		return jobs, err
	}

	for rows.Next() {
		var (
			id         int64
			name       string
			imageID    string
			userID     string
			status     string
			dispatched string
			started    string
			completed  string
			result     string
		)
		err := rows.Scan(&id, &userID, &name, &imageID, &status, &dispatched, &started, &completed, &result)

		if err != nil {
			return jobs, err
		}

		jobs = append(jobs, model.Job{
			ID:         id,
			UserID:     userID,
			Name:       name,
			ImageID:    imageID,
			Status:     status,
			Dispatched: dispatched,
			Started:    started,
			Completed:  completed,
			Result:     result,
		})
	}

	return jobs, nil
}

// UpdateJobByID update job
func (repo *JobMariaDBRepository) UpdateJobByID(userID string, id int64, job model.Job) (model.Job, error) {
	db := repo.openDatabase()
	defer db.Close()
	stmt, err := db.Prepare("UPDATE t_job SET status=?, dispatched=?, started=?, completed=? WHERE id=? AND userID=?")
	if err != nil {
		return job, err
	}

	_, err = stmt.Exec(job.Status, job.Dispatched, job.Started, job.Completed, job.ID, userID)

	return job, err
}

// DeleteJobByID delete job
func (repo *JobMariaDBRepository) DeleteJobByID(userID string, id int64) (int64, error) {
	db := repo.openDatabase()
	defer db.Close()

	stmt, err := db.Prepare("DELETE FROM t_job WHERE userID=? AND id=?")
	if err != nil {
		return 0, err
	}
	result, err := stmt.Exec(userID, id)

	return result.RowsAffected()
}

// DeleteJobByImageID delete all job with image id
func (repo *JobMariaDBRepository) DeleteJobByImageID(imageID string) (int64, error) {
	return 0, nil
}

func (repo *JobMariaDBRepository) openDatabase() *sql.DB {
	db, err := sql.Open(mariadbDriverName, repo.dsn)
	db.SetConnMaxIdleTime(time.Minute * 3)
	if err != nil {
		log.Fatalf("Failed to open database:\n%s", err)
	}
	return db
}
