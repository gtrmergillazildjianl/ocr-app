package repository

import (
	"time"
)

// StateRepository state storage
type StateRepository interface {
	StoreState(key string, state map[string]string, expiration time.Duration) error
	GetState(key string) (map[string]string, error)
	DeleteState(key string) error
}
