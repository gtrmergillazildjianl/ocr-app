package repository

// ImageRepository image data store
type ImageRepository interface {
	StoreImage(path string, data []byte, contentType string) error
	GetImage(path string) ([]byte, error)
	DeleteImage(path string) ([]byte, error)
}
