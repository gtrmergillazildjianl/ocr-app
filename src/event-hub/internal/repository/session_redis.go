package repository

import (
	"encoding/json"

	"ocrapp/src/event-hub/internal/model"

	"github.com/go-redis/redis/v8"
)

var sessionBaseKey = "session"
var bearerBaseKey = "bearer"

// SessionRedisRepository session repository redis implementation
type SessionRedisRepository struct {
	redisClient *redis.Client
}

// NewSessionRedisRepository returns redis session repository
func NewSessionRedisRepository() SessionRepository {
	return &SessionRedisRepository{
		redisClient: getRedisClient(),
	}
}

// GetSession from redis
func (repo *SessionRedisRepository) GetSession(key string) (model.Session, error) {
	sess := model.Session{}
	status := repo.redisClient.Get(redisCtx, sessionBaseKey+":"+key)

	if status.Err() != nil {
		return sess, status.Err()
	}

	bytes, err := status.Bytes()
	if err != nil {
		return sess, nil
	}

	json.Unmarshal(bytes, &sess)

	return sess, nil
}
