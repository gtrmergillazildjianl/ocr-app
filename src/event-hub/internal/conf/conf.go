package conf

import (
	"io/ioutil"

	"log"

	"gopkg.in/yaml.v2"
)

// Config - Api Configuration
type Config struct {
	ListenAddress string `yaml:"listenAddress"`
	Redis         struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		Password string `yaml:"password"`
	} `yaml:"redis"`
	NSQ struct {
		NSQLookupDHosts []string `yaml:"nsqlookupdHosts"`
	} `yaml:"nsq"`
}

var (
	instance *Config
)

// LoadConfig - Load Api Configuration
func LoadConfig(path string) *Config {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Failed to load configuration file: %s\n", path)
		log.Fatal(err)
	}

	config := Config{}
	err = yaml.Unmarshal([]byte(data), &config)

	if err != nil {
		log.Printf("Failed to Serialize configuration file should be a valid yaml: \n%s", string(data))
		log.Fatal(err)
	}

	log.Printf("Loaded Configuration file: %s\n%s\n", path, string(data))
	instance = &config

	return instance
}

// GetConfig - Return Loaded Configuration
func GetConfig() *Config {
	return instance
}
