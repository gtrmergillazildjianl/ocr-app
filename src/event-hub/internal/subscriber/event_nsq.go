package subscriber

import (
	"log"
	"net"

	"github.com/gobwas/ws/wsutil"

	"ocrapp/src/event-hub/internal/conf"

	"ocrapp/src/event-hub/pkg/random"

	"github.com/nsqio/go-nsq"
)

const (
	nsqEventTopicSuffix  = "_ocrapp_event#ephemeral"
	nsqEventChannelSuffx = "#ephemeral"
)

// WebSocketEventNSQSubscriber subscriber
type WebSocketEventNSQSubscriber struct {
	nsqSub *nsq.Consumer
	wsConn net.Conn
}

// WebSocketNSQHandler websocket handler
type WebSocketNSQHandler struct {
	subscriber *WebSocketEventNSQSubscriber
}

// NewWebSocketEventNSQSubscriber return subscriber
func NewWebSocketEventNSQSubscriber(conn net.Conn, userID string) (*WebSocketEventNSQSubscriber, error) {

	channel := random.GenerateBase64String(64) + nsqEventChannelSuffx
	consumer, err := newNSQConsumer(userID+nsqEventTopicSuffix, channel[len(channel)-64:])
	return &WebSocketEventNSQSubscriber{
		nsqSub: consumer,
		wsConn: conn,
	}, err
}

// Work do work
func (subscriber *WebSocketEventNSQSubscriber) Work() {
	subscriber.nsqSub.AddHandler(&WebSocketNSQHandler{
		subscriber: subscriber,
	})

	subscriber.nsqSub.ConnectToNSQLookupds(conf.GetConfig().NSQ.NSQLookupDHosts)

	// Spawn routine for watching client messages and client disconnect
	go func() {
		defer subscriber.nsqSub.Stop()
		defer subscriber.wsConn.Close()
		for {
			data, err := wsutil.ReadClientText(subscriber.wsConn)
			if err != nil {
				log.Println(err)
				subscriber.nsqSub.Stop()
				log.Println("Disconnecting client")
				return
			}
			log.Println(data)
		}
	}()
}

// HandleMessage handle
func (handler *WebSocketNSQHandler) HandleMessage(message *nsq.Message) error {
	err := wsutil.WriteServerText(handler.subscriber.wsConn, message.Body)

	if err != nil {
		log.Println(err)
		handler.subscriber.wsConn.Close()
		handler.subscriber.nsqSub.Stop()
		return err
	}
	return nil
}
