package subscriber

import (
	"time"

	"github.com/nsqio/go-nsq"
)

func newNSQConsumer(topic string, channel string) (*nsq.Consumer, error) {

	nsqConfig := nsq.NewConfig()
	nsqConfig.MaxInFlight = 100
	nsqConfig.LookupdPollInterval = 2 * time.Second

	return nsq.NewConsumer(topic, channel, nsqConfig)

}
