package model

// User details for user
type User struct {
	ID         uint64 `json:"id"`
	GoogleID   string `json:"google_id"`
	Email      string `json:"email"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	Picture    string `json:"picture"`
}
