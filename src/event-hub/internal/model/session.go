package model

// Session data to store
type Session struct {
	ID    string      `json:"id"`
	Token GoogleToken `json:"token"`
	User  User        `json:"user"`
}

// IsValid validates Session struct
func (sess Session) IsValid() (bool, map[string]string) {
	errors := make(map[string]string)
	return true, errors
}
