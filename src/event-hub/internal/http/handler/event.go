package handler

import (
	"fmt"
	"log"
	"net/http"
	"ocrapp/src/event-hub/internal/repository"
	"ocrapp/src/event-hub/internal/subscriber"
	"strings"

	"github.com/gobwas/ws"
)

// NewUpgradeHandler upgrade to ws connection
func NewUpgradeHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == "/health" {
			w.WriteHeader(200)
			return
		}

		sessionCookie, err := r.Cookie("session_id")
		if err != nil {
			w.WriteHeader(401)
			fmt.Fprint(w, "Invalid Session")
			return
		}
		sessionIDSlice := strings.Split(sessionCookie.Value, ":")
		if len(sessionIDSlice) != 2 {
			w.WriteHeader(403)
			fmt.Fprint(w, "Invalid Session")
		}
		session, err := repository.NewSessionRedisRepository().GetSession(sessionIDSlice[0])
		if err != nil || session.ID != sessionCookie.Value {
			w.WriteHeader(403)
			fmt.Fprint(w, "Invalid Session")
			return
		}

		conn, _, _, err := ws.UpgradeHTTP(r, w)
		if err != nil {
			log.Printf("Failed to upgrade to ws connection: \n%s\n", err)
			return
		}

		sub, err := subscriber.NewWebSocketEventNSQSubscriber(conn, session.User.GoogleID)

		if err != nil {
			conn.Close()
			log.Println(err)
			fmt.Fprint(w, "Server error: 500")
			return
		}
		sub.Work()
	})
}
