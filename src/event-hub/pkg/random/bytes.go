package random

import (
	"crypto/rand"
	"encoding/base64"
	"log"
)

// GenerateRandomBytes returns random array of bytes with given size
func GenerateRandomBytes(size int) []byte {
	out := make([]byte, size)
	_, err := rand.Read(out)
	if err != nil {
		log.Printf("Failed to generate random bytes: %s\n", err)
		return GenerateRandomBytes(size)
	}
	return out
}

// GenerateBase64String returns random base64string based on byte size
func GenerateBase64String(size int) string {
	return base64.RawURLEncoding.EncodeToString(GenerateRandomBytes(size))
}
