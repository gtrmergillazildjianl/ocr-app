import { Component } from "react";
import '../../css/Components/Login/SignInButton.css'
import googleLogo from "../../assets/icons/google-logo.svg"
import { Redirect } from "react-router-dom";


interface SignInState {
    LoginComplete: boolean
}
export default class SignInButton extends Component <any, SignInState>{
    constructor(props: any) {
        super(props)
        this.state = {
            LoginComplete: false
        }
    }
    render() {
        if (this.state.LoginComplete)
        return (
            <Redirect to="/"/>
        )

        return (    
            <button className="sign-in-button" onClick={this.launchLoginWindow.bind(this)}>
                <img src={googleLogo} alt="google-logo"/>
                <span>Sign In with Google</span>
            </button>
        );
    }


    private launchLoginWindow() {
        window.open("/api/auth/google/web/redirect", "_blank", "fullscreen=no,height=720,width=420")
    }
}