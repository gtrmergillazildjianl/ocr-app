import { Component} from 'react';
import '../../css/Components/Login/Footer.css';

export default class Footer extends Component {
    render() {
        return (
            <span className="copyright-footer">Copyright © 2021 by ageekhub.com</span>
        )
    }
}