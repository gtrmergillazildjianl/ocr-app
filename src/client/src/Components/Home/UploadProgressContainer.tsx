import { Component } from "react";
import UploadStore, { IUploadStoreState, IUpload } from "../../Stores/UploadStore";
import UploadProgress from './UploadProgress'
import "../../css/Components/Home/UploadProgressContainer.css"


interface IUploadProgressContainerProps {
    
}

export default class UploadProgressContainer extends Component <IUploadProgressContainerProps, IUploadStoreState>{
    constructor(props: IUploadProgressContainerProps) {
        super(props)
        this.state = {
            uploads: new Map<string, IUpload>(UploadStore.GetStore().getState().uploads)
        }
    }
    render() {
        let uploads = new Array<JSX.Element>()
        
        if (this.state !== null) {
            
            this.state.uploads.forEach((upload) => {
                uploads.push(<UploadProgress id={upload.id} name={upload.fileName} key={upload.id}/>)
            })
        }
        return (
            <div className="upload-progress-container">
                {uploads}
            </div>
        )
    }

    componentDidMount() {
        let store = UploadStore.GetStore()
        store.subscribe(() => {
            let state  = store.getState()
            if (this.state.uploads.size !== state.uploads.size) {
                this.setState({
                    uploads: new Map(state.uploads)
                })
            }
        })
    }
}