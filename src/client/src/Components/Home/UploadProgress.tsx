import { Component } from "react";
import UploadStore from "../../Stores/UploadStore";
import "../../css/Components/Home/UploadProgress.css"

interface IUploadProgressState {
    progress: number;
    status: "pending" | "failed";
}

interface IUploadProgressProps{
    id: string
    name: string
}
/**
 * 
 */
export default class UploadProgress extends Component<IUploadProgressProps, IUploadProgressState> {
    constructor(props: IUploadProgressProps) {
        super(props)
        this.state = {progress: 0, status: "pending"}
    }
    render() {
        return (
            <div id={this.props.id} className={`upload-progress-bar ${this.state.status === "failed" ? "upload-progress-failed":null}`} >
                <div className="upload-progress-indicator" style={{width: `${this.state.progress}%`, opacity: `${this.state.status === "failed" ? "0%":"100%"}`}}></div>
                <span className="name">{this.props.name}</span>
                <span className="percentage">{this.state.progress}%</span>
                <span className="delete" onClick={this.delete.bind(this)} style={{display: `${this.state.status === "failed" ? "":"none"}`}}>X</span> 
            </div>
        )
    }

    componentDidMount() {
        let store = UploadStore.GetStore()
        let unsub = store.subscribe(() => {
            let state = store.getState().uploads.get(this.props.id)
            if (state === undefined) {
                unsub()
                return
            }
            
            if (state.progress !== this.state.progress) {
                this.setState({
                    progress: state.progress,
                })
            }
            if (state.status !== this.state.status) {
                this.setState({
                    status: state.status
                })
            }
        })
    }

    private delete(event: React.MouseEvent<HTMLSpanElement, MouseEvent>) {
        UploadStore.GetStore().dispatch({
            type: "clear",
            upload: {
                fileName: "",
                id: this.props.id,
                progress: 0,
                status: "pending"
            }
        })
    }
}