import {createStore, Reducer, Action, Store} from "redux"

export interface IUploadStoreAction {
    type: "clear" | "push" | "update"
    upload: IUpload
}

export interface IUploadStoreState {
    uploads: Map<string, IUpload>
}

export interface IUpload {
    id: string
    fileName: string
    progress: number
    status: "pending" | "failed"
}

export default class UploadStore {
    private static Store: Store<IUploadStoreState, IUploadStoreAction>

    public static GetStore() {
        if (this.Store === undefined || this.Store === null) {
            this.Store = createStore(UploadStoreReducer)
        }
        return this.Store
    }
}

function UploadStoreReducer(state: IUploadStoreState | undefined, action: IUploadStoreAction) : IUploadStoreState{
    
    if (state === undefined) {
        state={uploads: new Map<string, IUpload>()}
    }

    switch (action.type) {
        case "clear":
            state.uploads.delete(action.upload.id)
            break;
        case "push":
            state.uploads.set(action.upload.id, action.upload)
            break;
        case "update":
            state.uploads.set(action.upload.id, action.upload)
            break;
        default:
            return state
    }       

    return state
}

