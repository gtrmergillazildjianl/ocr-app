import { createStore, Store } from "redux";


export interface IJobStoreState {
    oldestID: number,
    searchString?: string,
    jobs: Map<number,IJob>
}

// ID         int64  `json:"id"`
// UserID     string `json:"user_id"`
// ImageID    string `json:"image_id"`
// Status     string `json:"status"`
// Dispatched string `json:"dispatched"`
// Started    string `json:"started"`
// Completed  string `json:"completed"`
// Result     string `json:"result"`

// // JobStatusPending pending job
// JobStatusPending = "pending"
// // JobStatusInProgress job currently in progress
// JobStatusInProgress = "inprogress"
// // JobStatusDone job completed
// JobStatusDone = "done"
// // JobStatusFailed job failed
// JobStatusFailed = "failed"
export interface IJob {
    id: number,
    userID: string,
    name: string,
    imageID: string,
    status: "pending" | "inprogress" | "done" | "failed" | string,
    dispatched: string,
    started: string,
    completed: string,
    result: string,
    timestamp: number,
}

export interface IJobStoreAction {
    type: "set" | "delete" | "search"
    searchString?: string
    job?: IJob
}

// 
export default class JobStore {
    private static Store: Store<IJobStoreState, IJobStoreAction>

    public static getStore():Store<IJobStoreState, IJobStoreAction> {
        if (this.Store === null || this.Store === undefined) {
            this.Store = createStore(JobReducer)

        }
        return this.Store
    }
}

class realState {
    private static state: IJobStoreState
    public static GetState() {
        return realState.state
    }
    public static SetState(state: IJobStoreState) {
        realState.state = state
    }
}


function JobReducer(state: IJobStoreState | undefined, action: IJobStoreAction) : IJobStoreState {
    if (state === undefined) {
        state = {oldestID: -1, jobs: new Map<number, IJob>(), searchString: ""}
        realState.SetState(state)
    }

    if (state.searchString !== undefined || state.searchString !== null || state.searchString !== "") {
        state = realState.GetState()
    }

    switch (action.type) {
        case "set":
            if (action.job === undefined) break;
            let currentState = state.jobs.get(action.job.id)
            if (currentState == undefined || currentState.timestamp <= action.job.timestamp) {
                state.jobs.set(action.job.id, action.job)
                state.oldestID = (action.job.id < state.oldestID || state.oldestID === -1) ? action.job.id : state.oldestID
            }
            break;
        case "delete":
            if (action.job === undefined) break;
            state.jobs.delete(action.job.id)
            break;
        case "search":
            state.searchString = action.searchString
            break;
        default:
            return state;
    }
    if (state.searchString !== undefined || state.searchString !== null || state.searchString !== "") {
        realState.SetState(state)
        return {
            jobs: searchJob(state.searchString!, state.jobs),
            oldestID: state.oldestID,
            searchString: state.searchString,
        } as IJobStoreState
    }
    
    return state
}

function searchJob (searchString: string, jobs: Map<number, IJob>):  Map<number, IJob> {
    let jobsMatched = new Map<number, IJob>()
    jobs.forEach(job => {
        if (job.name.toLowerCase().match(searchString.toLowerCase()) !== null) {
            jobsMatched.set(job.id, job)
            return
        } else if (job.result.toLowerCase().match(searchString.toLowerCase()) !== null) {
            jobsMatched.set(job.id, job)
            return
        } else if (job.result.toLowerCase().match(searchString.toLowerCase()) !== null ){
            jobsMatched.set(job.id, job)
            return
        }
    })
    return jobsMatched
}