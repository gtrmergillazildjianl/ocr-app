import User from "./User";

/**
 * Bearer Schema
 * 
 * @author zildjian
 * @since 2021.01.01
 */
export default interface Bearer {
    User: User
    Bearer: string
}