import { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';
import './css/App.css';
import AuthMiddleware from './Components/Middleware/AuthMiddleware';
import Home from './Routes/Home';
import Login from './Routes/Login';
import NotFound from './Routes/NotFound';

export default class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/login" exact={true}>
            <Login/>
          </Route>
          <Route path="/" exact={true}>
            <AuthMiddleware>
              <Home/>
            </AuthMiddleware>
          </Route>
          <Route>
            <NotFound/>
          </Route>
        </Switch>
      </BrowserRouter>
    );
  }


}
