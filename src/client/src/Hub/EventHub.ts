import IJobSchema from "../Api/Schemas/Job";
import JobStore, { IJob } from "../Stores/JobStore"

// // JobEventTypeFailed job failed event
// JobEventTypeFailed = "failed"
// // JobEventTypeDispatched job dispatched
// JobEventTypeDispatched = "dispatched"
// // JobEventTypeInProgress job inprogress
// JobEventTypeInProgress = "inprogress"
// // JobEventTypeSuccess job success
// JobEventTypeSuccess = "success"
// // JobEventTypeDeleted job deleted
// JobEventTypeDeleted = "deleted"
// // JobEventTypeUpdated job updated
// JobEventTypeUpdated = "updated"
// ID         int64  `json:"id"`
// UserID     string `json:"user_id"`
// ImageID    string `json:"image_id"`
// Status     string `json:"status"`
// Dispatched string `json:"dispatched"`
// Started    string `json:"started"`
// Completed  string `json:"completed"`
// Result     string `json:"result"`
interface JobEvent {
    type: "failed" | "dispatched" | "inprogress" | "success" | "deleted" | "updated"
    timestamp: string
    job: IJobSchema
}

/**
 * EventHub
 * 
 * connects to websocket eventhub
 */
export default class EventHub {

    private path = "/hub/event/"
    private connection!: WebSocket;
    private static instance: EventHub;

    private constructor() {}

    public static GetInstance(): EventHub {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new EventHub()
        }
        return this.instance
    }


    private connect() {
        let url = window.location
        let protocol = url.protocol === "https:" ? "wss":"ws"
        this.connection = new WebSocket(`${protocol}://${url.hostname}${this.path}`)
        this.bindEventHandlers()
    }

    private bindEventHandlers() {
        this.connection.onclose = () => {
            this.connect()
        }
        this.connection.onmessage = (message) => {
            let data = JSON.parse(message.data) as JobEvent
            
            if (data.type === "dispatched" || data.type === "inprogress" || data.type === "failed" || data.type === "success" || data.type === "updated") {    
                JobStore.getStore().dispatch({
                    type: "set",
                    job: {
                        id: data.job.id,
                        userID: data.job.user_id,
                        name: data.job.name,
                        imageID: data.job.image_id,
                        timestamp: parseInt(data.timestamp, 10),
                        status: data.job.status,
                        dispatched: data.job.dispatched,
                        started: data.job.started,
                        completed: data.job.completed,
                        result: data.job.result,
                    }
                })
            } else if (data.type === "deleted") {
                JobStore.getStore().dispatch({
                    type: 'delete',
                    job: {
                        id: data.job.id,
                        userID: data.job.user_id,
                        name: data.job.name,
                        imageID: data.job.image_id,
                        timestamp: parseInt(data.timestamp, 10),
                        status: data.job.status,
                        dispatched: data.job.dispatched,
                        started: data.job.started,
                        completed: data.job.completed,
                        result: data.job.result,
                    }
                })
            }
        }
    }

    public startListening() {
        if (this.connection !== null && this.connection !== undefined) {
            this.connection.close()
        }
        this.connect();
    }
    

    public close() {
        this.connection.onclose = () => {}
        this.connection.close()
    }

}