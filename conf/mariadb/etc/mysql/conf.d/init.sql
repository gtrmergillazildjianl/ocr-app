CREATE DATABASE ocrapp;
USE ocrapp;
CREATE TABLE t_job(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    userID VARCHAR(64) NOT NULL,
    name TEXT NOT NULL,
    imageID VARCHAR(128) NOT NULL,
    status VARCHAR(20),
    dispatched TIMESTAMP,
    started TIMESTAMP,
    completed TIMESTAMP,
    result TEXT NOT NULL DEFAULT ""
);