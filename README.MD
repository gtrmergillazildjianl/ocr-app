# OCR Application

## Local Dev Environment


### Requirements
```
docker
docker-compose
```

### Run
```
docker-compose up
```

You can now visit https://localhost once the installation of dependencies is complete.

It is recommended to run this on linux environment, mine is running on wsl2 ubuntu (I shifted to ubuntu later on because of resource issue in wsl2 after a windows update >.<).


## Features

- secured google signin (authorization code flow), the application has no means off storing user's credentials except the username and openid details.
- user can upload images, an available backend processor will extract the texts from the uploaded images
- supports jpeg/png images, an invalid file will not be accepted by the server
- lazy loads results while scrolling
- user can delete uploaded images
- user can search result/filename using the searchbox
- signout
- results are synchronized between user sessions, homepage is updated via a websocket connection that listens to events

## Architecture

![architecture](docs/architecture.png)

## Want to Contribute?

Contributions are welcome :D

# Contributors
| | | | |
| - | - | - | - |
| ![zildjian](https://lh3.googleusercontent.com/a-/AOh14GjMCb9GdPe3skgTpTMm0QyWhaYFBTFAKhJgQaJb8A=s96-c) | Zildjian L. Mergilla | gtrmergillazildjianl@gmail.com | **owner** |

