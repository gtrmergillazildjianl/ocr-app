module ocrapp

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v8 v8.4.4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/google/uuid v1.1.4
	github.com/gorilla/mux v1.8.0
	github.com/minio/minio-go/v7 v7.0.6
	github.com/nsqio/go-nsq v1.0.8
	github.com/otiai10/gosseract/v2 v2.3.1
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	gopkg.in/yaml.v2 v2.4.0
)
